using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QuizManager : MonoBehaviour
{
    public List<QuestionsAnswers> QnA; // List of questions and answers
    public GameObject[] options; // Array of answer options
    public int currentQuestion, score; // Current question and score
    public TMP_Text questionText, scoreText, finalScoreText, timer; // Text elements for questions, score, etc.
    public GameObject startScreen, gameScreen, nextBtn, finalScreen; // Objects for screens and 'Next' button
    [HideInInspector] public bool turnOff; // Variable for controlling the game
    int questionCounter; // Question counter
    public GameObject groupTwo;
    public GameObject groupFour;

    private int questionCount;
    private List<Button> buttons = new List<Button>();

    //time variables
    public float startTime, currentTime;
    bool questionTimeRunning;
    // Start method is called at the beginning of the game
    void Start()
    {
        currentTime = startTime;
        gameScreen.SetActive(false);
        startScreen.SetActive(true);
        GenerateQuestion();
        nextBtn.SetActive(false);
        finalScreen.SetActive(false);
        Time.timeScale = 0;
        questionCount = QnA.Count;
    }
    // Update method is called on every frame
    void Update()
    {
        scoreText.text = "Score: " + score; // Updates the score text
        finalScoreText.text = "Total score: " + score + "/" + questionCount; // Updates the total score text
        if (questionCounter >= questionCount + 1)
        {
            finalScreen.SetActive(true); // Shows the final screen
            nextBtn.SetActive(false); // Hides the 'Next' button
        }
        if (questionTimeRunning)
        {
            currentTime -= Time.deltaTime;
            timer.text = "Time: " + currentTime.ToString("0"); // Updates the timer
            if (currentTime <= 0)
            {
                currentTime = 0;
                questionTimeRunning = false;
                // Checks if the selected answer is correct and displays the answer
                if (options[0].GetComponent<AnswerScript>().isCorrect)
                {
                    options[1].GetComponent<AnswerScript>().Answer();
                }
                else
                {
                    options[0].GetComponent<AnswerScript>().Answer();
                }
            }
        }
    }

    void SetAnswer()
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<AnswerScript>().isCorrect = false;
            options[i].transform.GetChild(0).GetComponent<TMP_Text>().text = QnA[currentQuestion].answers[i];
            if (QnA[currentQuestion].correctAnswer == i + 1)
            {
                options[i].GetComponent<AnswerScript>().isCorrect = true;
            }
        }
    }

    public void UnlockButtons()
    {
        foreach (Button button in buttons)
        {
            button.interactable = true;
        }
    }

    public void ShowHideButtons(GameObject groupShow, GameObject groupHide)
    {
        groupShow.SetActive(true);
        groupHide.SetActive(false);
    }
    //this segment generates questions
    public void GenerateQuestion()
    {
        if (options.Length == 2)
        {
            ShowHideButtons(groupTwo, groupFour);
        }
        else
        {
            ShowHideButtons(groupFour, groupTwo);
        }
        Button[] foundButtons = FindObjectsOfType<Button>();
        buttons.AddRange(foundButtons);
        UnlockButtons();
        currentTime = startTime;
        
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<Image>().color = options[i].GetComponent<AnswerScript>().baseColor;
        }
        questionCounter++;
        turnOff = true;
        if (QnA.Count > 0)
        {
            questionTimeRunning = true;
            currentQuestion = Random.Range(0, QnA.Count);
            questionText.text = QnA[currentQuestion].question;
            nextBtn.SetActive(false);
            SetAnswer();
        }
        else Destroy(timer);
    }
    public void Correct()
    {
        questionTimeRunning = false;
        QnA.RemoveAt(currentQuestion);
        for (int i = 0; i < options.Length; i++)
        {
            if(options[i].GetComponent<AnswerScript>().isCorrect == true)
            {
                options[i].GetComponent<Image>().color = options[i].GetComponent<AnswerScript>().rightColor;
            }
        }
    }
    public void StartGame()
    {
        Time.timeScale = 1;
        startScreen.SetActive(false);
        gameScreen.SetActive(true);
    }
    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
