using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerScript : MonoBehaviour
{
    public bool isCorrect; // Indicates whether this answer is correct
    public QuizManager quizManager; // Reference to the QuizManager script
    public GameObject correct, wrong; // GameObjects for showing correct and wrong feedback
    public AudioSource feedbackSound; // Audio source for feedback sounds
    public AudioClip rightClip, wrongClip; // Audio clips for right and wrong answers
    public Color baseColor, rightColor, wrongColor; // Colors for answer feedback

    private List<Button> buttons = new List<Button>();
    // Start is called before the first frame update
    void Start()
    {
        correct.SetActive(false); // Initially, hide the 'correct' feedback object
        wrong.SetActive(false); // Initially, hide the 'wrong' feedback object
        // Find all buttons in the scene and add them to the 'buttons' list
        Button[] foundButtons = FindObjectsOfType<Button>();
        buttons.AddRange(foundButtons);
    }

    // Update is called once per frame
    void Update()
    {
        // If 'turnOff' in the QuizManager is set, hide both 'correct' and 'wrong' feedback objects
        if (quizManager.turnOff)
            if (quizManager.turnOff)
        {
            correct.SetActive(false);
            wrong.SetActive(false);
            quizManager.turnOff = false;
        }
    }

    public void BlockButtons()
    {
        // Disable all buttons in the 'buttons' list except the "Next" button
        foreach (Button button in buttons)
        {
            if (button.name != "Next")
            {
                button.interactable = false;
            }
        }
    }

    public void Answer()
    {
        if (isCorrect)
        {
            // If this answer is correct, change the color to 'rightColor', play a right sound, show 'correct' feedback, and increase the score
            gameObject.GetComponent<Image>().color = rightColor;
            feedbackSound.clip = rightClip;
            feedbackSound.Play();
            correct.SetActive(true);
            quizManager.score++;
        }
        else
        {
            // If this answer is wrong, change the color to 'wrongColor', play a wrong sound, and show 'wrong' feedback
            gameObject.GetComponent<Image>().color = wrongColor;
            feedbackSound.clip = wrongClip;
            feedbackSound.Play();
            wrong.SetActive(true);
        }
        quizManager.nextBtn.SetActive(true); // Enable the "Next" button in the QuizManager
        quizManager.Correct(); // Call the 'Correct' method in the QuizManager to handle the correct answer
        BlockButtons(); // Disable all answer buttons
    }
}
